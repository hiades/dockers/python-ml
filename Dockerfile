FROM python:3.11-alpine

LABEL name="python-3.11-alpine-ml" \
	maintainer="Hiades Patterns Solutions <ilorenzo@hiades.es>" \
	version="1.0" \
	description="Python 3.11 alpine ML"

ENV PIP_ROOT_USER_ACTION=ignore

SHELL ["/bin/sh", "-o", "pipefail", "-c"]

COPY requirements.txt /

# hadolint ignore=DL3008,DL3018
RUN apk add --no-cache --virtual .build-deps \
					   musl-dev \
					   g++ \
					   gcc && \
	pip install --no-cache-dir -r requirements.txt && \
	apk del .build-deps
